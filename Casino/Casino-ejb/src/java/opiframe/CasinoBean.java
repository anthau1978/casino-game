/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package opiframe;

import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.enterprise.context.*;




/**
 *
 * @author koulutus
 */
@Stateless
@LocalBean
public class CasinoBean {
    
   private int score=2;
   
   public CasinoBean()  {
       
   }
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    /**
     * @return the score
     */
    public int getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(int score) {
        this.score = score;
    }
}
